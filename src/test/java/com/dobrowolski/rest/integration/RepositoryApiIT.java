package com.dobrowolski.rest.integration;

import com.dobrowolski.rest.annotation.IntegrationTest;
import com.github.tomakehurst.wiremock.client.WireMock;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.AutoConfigureWebTestClient;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.ContentDisposition;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.reactive.server.WebTestClient;

import java.io.BufferedInputStream;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.zip.ZipInputStream;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

@ActiveProfiles("stub")
@Category(IntegrationTest.class)
@RunWith(SpringRunner.class)
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.DEFINED_PORT)
@org.springframework.cloud.contract.wiremock.AutoConfigureWireMock(port = 5000)
@AutoConfigureWebTestClient(timeout = "10000")
public class RepositoryApiIT {

    @Autowired
    private WebTestClient webTestClient;

    @Test
    public void testSuccessfulResponse() throws IOException {
        WireMock.stubFor(WireMock.get("/posts").willReturn(WireMock.okJson("[{\"userId\":1,\"id\":1,\"title\":\"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\",\"body\":\"suscipit recusandae consequuntur expedita et cum\"},{\"userId\":2,\"id\":2,\"title\":\"qui est esse\",\"body\":\"est rerum tempore vitae\"}]")));
        byte[] bytes = webTestClient.get()
                .uri("/posts")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .exchange()
                .expectHeader().contentLength(418)
                .expectHeader().contentDisposition(ContentDisposition.builder("attachment").filename("result.zip").build())
                .expectHeader().contentType(MediaType.APPLICATION_OCTET_STREAM)
                .expectStatus().isOk()
                .expectBody()
                .returnResult()
                .getResponseBody();

        assertTrue(bytes.length > 0);

        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(bytes);
        BufferedInputStream bufferedInputStream = new BufferedInputStream(byteArrayInputStream);
        ZipInputStream zipInputStream = new ZipInputStream(bufferedInputStream);
        assertEquals("1.json", zipInputStream.getNextEntry().getName());
        assertEquals("{\"userId\":1,\"id\":1,\"title\":\"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\",\"body\":\"suscipit recusandae consequuntur expedita et cum\"}", new String(zipInputStream.readAllBytes()));
        assertEquals("2.json", zipInputStream.getNextEntry().getName());
        assertEquals("{\"userId\":2,\"id\":2,\"title\":\"qui est esse\",\"body\":\"est rerum tempore vitae\"}", new String(zipInputStream.readAllBytes()));
    }

    @Test
    public void testNotFoundResponse() {
        WireMock.stubFor(WireMock.get("/posts").willReturn(WireMock.notFound().withBody("{\"reason\":\"Something went wrong\"}")));
        webTestClient.get()
                .uri("/posts")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isEqualTo(404)
                .expectBody()
                .jsonPath("$.path").isEqualTo("/posts")
                .jsonPath("$.status").isEqualTo("404")
                .jsonPath("$.error").isEqualTo("Not Found")
                .jsonPath("$.message").isEqualTo("{\"reason\":\"Something went wrong\"}");
    }

    @Test
    public void testUpstreamServerError() {
        WireMock.stubFor(WireMock.get("/posts").willReturn(WireMock.serverError()));
        webTestClient.get()
                .uri("/posts")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isEqualTo(502)
                .expectBody()
                .jsonPath("$.path").isEqualTo("/posts")
                .jsonPath("$.status").isEqualTo("502")
                .jsonPath("$.error").isEqualTo("Bad Gateway")
                .jsonPath("$.message").isEqualTo("We had trouble connecting to the JSONPlaceholder server.");
    }

    @Test
    public void testConnectionTimeout() {
        WireMock.stubFor(WireMock.get("/posts").willReturn(WireMock.aResponse().withFixedDelay(5000)));
        webTestClient.get()
                .uri("/posts")
                .header(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .exchange()
                .expectHeader().contentType(MediaType.APPLICATION_JSON_UTF8)
                .expectStatus().isEqualTo(504)
                .expectBody()
                .jsonPath("$.path").isEqualTo("/posts")
                .jsonPath("$.status").isEqualTo("504")
                .jsonPath("$.error").isEqualTo("Gateway Timeout")
                .jsonPath("$.message").isEqualTo("JSONPlaceholder did not respond on time.");
    }
}
