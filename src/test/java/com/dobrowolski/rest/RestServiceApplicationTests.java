package com.dobrowolski.rest;

import com.dobrowolski.rest.annotation.EndToEndTest;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

@Category(EndToEndTest.class)
@RunWith(SpringRunner.class)
@SpringBootTest
public class RestServiceApplicationTests {

    @Test
    public void contextLoads() {
    }

}
