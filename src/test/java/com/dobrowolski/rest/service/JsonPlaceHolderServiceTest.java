package com.dobrowolski.rest.service;

import com.dobrowolski.rest.RestServiceApplication;
import com.dobrowolski.rest.annotation.UnitTest;
import com.dobrowolski.rest.dto.Post;
import com.github.tomakehurst.wiremock.client.WireMock;
import io.netty.channel.ConnectTimeoutException;
import io.netty.handler.timeout.ReadTimeoutException;
import org.junit.Before;
import org.junit.Test;
import org.junit.experimental.categories.Category;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.context.annotation.Import;
import org.springframework.http.HttpStatus;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.test.StepVerifier;

import static org.mockito.ArgumentMatchers.notNull;
import static org.mockito.Mockito.when;
import static org.springframework.http.HttpStatus.*;

@ActiveProfiles("stub")
@Category(UnitTest.class)
@RunWith(SpringRunner.class)
@Import({RestServiceApplication.class})
@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@org.springframework.cloud.contract.wiremock.AutoConfigureWireMock(port = 5000)
public class JsonPlaceHolderServiceTest {

    @Autowired
    JsonPlaceHolderService jsonPlaceHolderService;

    @Test
    public void testSuccessfulResponse() {
        WireMock.stubFor(WireMock.get("/posts").willReturn(WireMock.okJson("[{\"userId\":1,\"id\":1,\"title\":\"sunt aut facere repellat provident occaecati excepturi optio reprehenderit\",\"body\":\"suscipit recusandae consequuntur expedita et cum\"},{\"userId\":2,\"id\":2,\"title\":\"qui est esse\",\"body\":\"est rerum tempore vitae\"}]")));
        StepVerifier.create(jsonPlaceHolderService.getPostsDetails())
                .expectNext(Post.builder().id(1).userId(1).title("sunt aut facere repellat provident occaecati excepturi optio reprehenderit").body("suscipit recusandae consequuntur expedita et cum").build())
                .expectNext(Post.builder().id(2).userId(2).title("qui est esse").body("est rerum tempore vitae").build())
                .verifyComplete();
    }

    @Test
    public void testNotFoundResponse() {
        WireMock.stubFor(WireMock.get("/posts").willReturn(WireMock.notFound().withBody("Something went wrong")));
        StepVerifier.create(jsonPlaceHolderService.getPostsDetails())
                .expectErrorMatches(throwable -> assertErrorMessage(throwable, NOT_FOUND, "Something went wrong"))
                .verify();
    }

    @Test
    public void testUpstreamServerError() {
        WireMock.stubFor(WireMock.get("/posts").willReturn(WireMock.serverError()));
        StepVerifier.create(jsonPlaceHolderService.getPostsDetails())
                .expectErrorMatches(throwable -> assertErrorMessage(throwable, BAD_GATEWAY, "We had trouble connecting to the JSONPlaceholder server."))
                .verify();
    }

    @Test
    public void testConnectionTimeout() {
        StepVerifier.create(new JsonPlaceHolderServiceImpl(createWebClientMockWithException(new ConnectTimeoutException())).getPostsDetails())
                .expectErrorMatches(throwable -> assertErrorMessage(throwable, GATEWAY_TIMEOUT, "JSONPlaceholder did not respond on time."))
                .verify();
    }

    @Test
    public void testReadTimeout() {
        StepVerifier.create(new JsonPlaceHolderServiceImpl(createWebClientMockWithException(ReadTimeoutException.INSTANCE)).getPostsDetails())
                .expectErrorMatches(throwable -> assertErrorMessage(throwable, GATEWAY_TIMEOUT, "JSONPlaceholder did not respond on time."))
                .verify();
    }

    private boolean assertErrorMessage(Throwable throwable, HttpStatus expectStatus, String expectReason) {
        if (throwable instanceof ResponseStatusException) {
            ResponseStatusException responseStatusException = (ResponseStatusException) throwable;
            return responseStatusException.getStatus().equals(expectStatus) && responseStatusException.getReason().equals(expectReason);
        }
        return false;
    }

    private WebClient createWebClientMockWithException(Exception exception) {
        final var webClient = Mockito.mock(WebClient.class);
        final var uriSpecMock = Mockito.mock(WebClient.RequestHeadersUriSpec.class);
        final var headersSpecMock = Mockito.mock(WebClient.RequestHeadersSpec.class);
        final var responseSpec = Mockito.mock(WebClient.ResponseSpec.class);
        when(webClient.get()).thenReturn(uriSpecMock);
        when(uriSpecMock.uri(Mockito.eq("/posts"))).thenReturn(headersSpecMock);
        when(headersSpecMock.header(notNull(), notNull())).thenReturn(headersSpecMock);
        when(headersSpecMock.headers(notNull())).thenReturn(headersSpecMock);
        when(headersSpecMock.retrieve()).thenReturn(responseSpec);
        when(responseSpec.onStatus(notNull(), notNull())).thenReturn(responseSpec);
        when(responseSpec.bodyToFlux(Post.class)).thenReturn(Flux.error(exception));
        return webClient;
    }

}
