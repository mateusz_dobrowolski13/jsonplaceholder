package com.dobrowolski.rest.handler;

import com.dobrowolski.rest.exception.ZipCreationException;
import com.dobrowolski.rest.service.JsonPlaceHolderService;
import com.fasterxml.jackson.databind.ObjectMapper;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.http.MediaType;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.BodyInserters;
import org.springframework.web.reactive.function.server.ServerRequest;
import org.springframework.web.reactive.function.server.ServerResponse;
import reactor.core.publisher.Mono;

import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

@Component
@Slf4j
@RequiredArgsConstructor
public class JsonPlaceHolderHandler {

    private final JsonPlaceHolderService jsonPlaceHolderService;
    private final ObjectMapper objectMapper;

    public Mono<ServerResponse> getPosts() {

        return ServerResponse.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header("Content-Disposition", "attachment; filename=\"result.zip\"")
                .body(BodyInserters.fromPublisher(createZipResource().map(ByteArrayOutputStream::toByteArray), byte[].class));
    }

    private Mono<ByteArrayOutputStream> createZipResource() {
        ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
        BufferedOutputStream bufferedOutputStream = new BufferedOutputStream(byteArrayOutputStream);
        ZipOutputStream zipOutputStream = new ZipOutputStream(bufferedOutputStream);
        return jsonPlaceHolderService.getPostsDetails()
                .collect(() -> zipOutputStream, (outputStream, post) -> {
                    try {
                        outputStream.putNextEntry(new ZipEntry(post.getId() + ".json"));
                        outputStream.write(objectMapper.writeValueAsString(post).getBytes());
                        outputStream.flush();
                        outputStream.closeEntry();
                    } catch (IOException e) {
                        throw new ZipCreationException("Problem with creating a zip file with Posts.");
                    }
                }).doOnSuccess(outputStream -> {
                    try {
                        outputStream.close();
                    } catch (IOException e) {
                        throw new ZipCreationException("The zip file has already been closed.");
                    }
                }).map(zip -> byteArrayOutputStream);
    }
}
