package com.dobrowolski.rest.dto;

import lombok.*;

@Builder
@NoArgsConstructor
@AllArgsConstructor
@Getter
@EqualsAndHashCode
public class Post {
    private int userId;
    private int id;
    private String title;
    private String body;
}
