package com.dobrowolski.rest.service;

import com.dobrowolski.rest.dto.Post;
import io.netty.channel.ConnectTimeoutException;
import io.netty.handler.timeout.ReadTimeoutException;
import lombok.RequiredArgsConstructor;
import org.springframework.core.io.buffer.DataBufferUtils;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.web.reactive.function.BodyExtractors;
import org.springframework.web.reactive.function.client.ClientResponse;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.server.ResponseStatusException;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import static org.springframework.http.HttpStatus.BAD_GATEWAY;
import static org.springframework.http.HttpStatus.GATEWAY_TIMEOUT;

@Service
@RequiredArgsConstructor
class JsonPlaceHolderServiceImpl implements JsonPlaceHolderService {

    private final WebClient webClient;

    public Flux<Post> getPostsDetails() {
        return webClient.get()
                .uri("/posts")
                .retrieve()
                .onStatus(HttpStatus::is4xxClientError, this::getResponseStatusException)
                .onStatus(HttpStatus::is5xxServerError, clientResponse -> Mono.error(new ResponseStatusException(BAD_GATEWAY, "We had trouble connecting to the JSONPlaceholder server.")))
                .bodyToFlux(Post.class)
                .doOnError(throwable -> throwable instanceof ReadTimeoutException || throwable instanceof ConnectTimeoutException, e -> {
                    throw new ResponseStatusException(GATEWAY_TIMEOUT, "JSONPlaceholder did not respond on time.");
                });
    }

    private Mono<ResponseStatusException> getResponseStatusException(ClientResponse clientResponse) {
        return DataBufferUtils.join(clientResponse.body(BodyExtractors.toDataBuffers()))
                .map(dataBuffer -> {
                    byte[] bytes = new byte[dataBuffer.readableByteCount()];
                    dataBuffer.read(bytes);
                    DataBufferUtils.release(dataBuffer);
                    return bytes;
                })
                .defaultIfEmpty(new byte[0])
                .map(bytes -> new ResponseStatusException(clientResponse.statusCode(), new String(bytes)));
    }
}
