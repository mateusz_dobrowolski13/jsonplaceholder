package com.dobrowolski.rest.service;

import com.dobrowolski.rest.dto.Post;
import reactor.core.publisher.Flux;

public interface JsonPlaceHolderService {
    Flux<Post> getPostsDetails();
}
