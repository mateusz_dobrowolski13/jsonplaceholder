package com.dobrowolski.rest.config;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.ExchangeFilterFunction;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.core.publisher.Mono;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

@Configuration
@Slf4j
class WebClientConfig {
    private static final String USER_AGENT = "Json REST Service";

    @Value("${jsonplaceholder.url}")
    private String url;

    @Value("${jsonplaceholder.connectionTimeout}")
    private int connectionTimeout;

    @Value("${jsonplaceholder.readTimeout}")
    private int readTimeout;

    @Value("${jsonplaceholder.writeTimeout}")
    private int writeTimeout;

    @Bean
    public WebClient webClient() {
        return WebClient
                .builder()
                .baseUrl(url)
                .defaultHeader(HttpHeaders.CONTENT_TYPE, MediaType.APPLICATION_JSON_UTF8_VALUE)
                .defaultHeader(HttpHeaders.USER_AGENT, USER_AGENT)
                .clientConnector(new ReactorClientHttpConnector(HttpClient.from(timeoutClient())))
                .filter(logServerErrorCodes())
                .build();
    }

    private TcpClient timeoutClient() {
        return TcpClient.create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, connectionTimeout * 1000)
                .doOnConnected(
                        connection -> connection.addHandlerLast(new ReadTimeoutHandler(writeTimeout))
                                                .addHandlerLast(new WriteTimeoutHandler(readTimeout)));
    }

    private ExchangeFilterFunction logServerErrorCodes() {
        return ExchangeFilterFunction.ofResponseProcessor(clientResponse -> {
            if (clientResponse.statusCode().is5xxServerError()) {
                return clientResponse.toEntity(String.class)
                        .doOnNext(responseEntity -> log.error("Status={}, Body={}", responseEntity.getStatusCode(), responseEntity.getBody()))
                        .flatMap(responseEntity -> Mono.just(clientResponse));
            }
            return Mono.just(clientResponse);
        });
    }
}
