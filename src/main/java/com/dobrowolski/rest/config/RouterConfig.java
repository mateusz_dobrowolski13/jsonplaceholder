package com.dobrowolski.rest.config;

import com.dobrowolski.rest.handler.JsonPlaceHolderHandler;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.reactive.function.server.RouterFunction;
import org.springframework.web.reactive.function.server.RouterFunctions;

import static org.springframework.web.reactive.function.server.RequestPredicates.GET;

@Configuration
@RequiredArgsConstructor
class RouterConfig {

    private final JsonPlaceHolderHandler jsonPlaceHolderHandler;

    @Bean
    public RouterFunction route() {
        return RouterFunctions
                .route(GET("/posts"), request -> jsonPlaceHolderHandler.getPosts());
    }

}
