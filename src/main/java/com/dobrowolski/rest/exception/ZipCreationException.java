package com.dobrowolski.rest.exception;

public class ZipCreationException extends RuntimeException {

    public ZipCreationException(String message) {
        super(message);
    }
}
